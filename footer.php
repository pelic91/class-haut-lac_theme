<?php
/**
 * Footer.php outputs the code for your footer widgets, contains your footer hook and closing body/html tags
 * @package Tetris WordPress Theme
 * @since 1.0
 * @author AJ Clarke : http://wpexplorer.com
 * @copyright Copyright (c) 2012, AJ Clarke
 * @link http://wpexplorer.com
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */


$base = get_stylesheet_directory_uri();

?>

<div class="clear"></div><!-- /clear any floats -->
</div><!-- /wrap -->
</div><!-- /main-content -->
<div id="green_footer_section" class="footer_side " xmlns="http://www.w3.org/1999/html">
    <div class=" container text_title">
        <div class="col-md-3">
            <img src="<?= $base; ?>/images/logo_footter.png" class="" alt="logo_footter">
        </div>
        <div class="col-sm-9">

            <p>EXPERIENCE THE HAUT-LAC <strong>LEARNING </strong>ENVIRONMENT</p>
        </div>
    </div>
</div>

<div id="blue_footer_section" class="blue_footer">
    <div class="angle_image">
        <img width="1920" height="45" src="<?= $base; ?>/images/test.png" class="vc_single_image-img attachment-full"
             alt="test"
             srcset="<?= $base; ?>/images/test.png, /<?= $base; ?>/images/test.png, <?= $base; ?>/images/test.png, <?= $base; ?>/images/test.png"
             sizes="(max-width: 1920px) 100vw, 1920px">
    </div>

    <div class="container">
        <div class="footer_box_1 col-md-3">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="fa fa-plus"></i>
                                      Primary Campus

                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <p>
                                        Ch. de Pangires 26</br>
                                        CH - 1806 St-Légier-la Chiésaz</br>
                                        <a href="tel:0215555000"> Phone - 021 555 50 00</a><br/>
                                        Fax – 021 555 50 01<br>
                                        Email – info@haut-lac.ch
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="fa fa-plus"></i>
                                        Secondary Campus
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p>
                                        Route du Tirage, 14<br/>
                                        CH - 1806 St-Légier-la Chiésaz<br/>
                                        <a href="tel:0215555000"> Phone - 021 555 50 00</a><br/>
                                        Fax – 021 555 50 01<br>
                                        Email – info@haut-lac.ch
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="fa fa-plus"></i>
                                        Crèche
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p>Crèche<br>
                                        Rue du Nord, 3-5<br/>
                                        CH - 1800 Vevey<br/>
                                        <a href="tel:0215555000"> Phone - 021 555 50 00</a><br/>
                                        Fax – 021 555 50 01<br/>
                                        Email – info@haut-lac.ch
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="footer_box_2 col-md-3">
            <h5>Follow us With</h5>
            <p>
                <a href="https://www.facebook.com/hautlac/" target="_blank"><i class="fa fa-facebook"></i> Facebook</a><br>
                <a href="https://twitter.com/HautLac" target="_blank"><i class="fa fa-twitter"></i> Twitter</a><br>
                <a href="https://www.linkedin.com/company/haut-lac-international-bilingual-school" target="_blank"><i class="fa fa-linkedin"></i> Linkedin</a><br>
            </p>



        </div>
        <div class="footer_box_3 col-md-3">

        </div>
        <div class="footer_box_4 col-md-3">


        </div>
    </div>

    <div class="wpb_single_image wpb_content_element vc_align_center">
        <figure class="wpb_wrapper vc_figure">
            <img width="529" height="234" src="<?= $base; ?>/images/mountitns.png"
                 class="vc_single_image-img attachment-full" alt="mountitns"
                 srcset="<?= $base; ?>/images/mountitns.png, <?= $base; ?>/images/mountitns.png"
                 sizes="(max-width: 529px) 100vw, 529px">

        </figure>
    </div>
</div>

<div id="copyright_section" class=" copyright">
    <div class="container">
        <h2>&copy; <?= date("Y"); ?> haut-lac.ch, Tous droits réservés</h2>
    </div>
</div>

<?php wp_footer(); // Footer hook, do not delete, ever ?>
</body>
</html>