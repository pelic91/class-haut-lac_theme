<?php
/**
 * The Header for our theme.
 *
 * @package WordPress
 * @subpackage Tetris WPExplorer Theme
 * @since Tetris 1.0
 */


$base = get_stylesheet_directory_uri();
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php if ( get_theme_mod('wpex_custom_favicon') ) { ?>
		<link rel="shortcut icon" href="<?php echo get_theme_mod('wpex_custom_favicon'); ?>" />
	<?php } ?>
	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>
<!-- Begin Body -->
<body <?php body_class(); ?>>




    <header>
        <div class="container_full_size">
            <div class="container">
                <div class="header_sector">
                    <div class="col-md-3">
                        <div class="logo">
                            <a href="<?php echo get_option('home'); ?>"><img src="<?= $base ?>/images/logo.png" alt=" <?php bloginfo('name'); ?>" title=" <?php bloginfo('name'); ?>" class="img-responsive"></a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-8" style="text-align: center">
                    <span class="simple_text">
                      <?php echo get_bloginfo('description'); ?>
                    </span>
                        </div>
                        <div class="col-md-4">

                            <div class="info">
                                <div class="telephone_number">
                                    <span class="glyphicon glyphicon-phone-alt"></span> +41 21 555 5000
                                </div>
                                <div class="social">
                                    <a href="https://www.facebook.com/hautlac/" target="_blank"><i class="fa fa-facebook"></i></a>
                                    <a href="https://twitter.com/HautLac" target="_blank"><i class="fa fa-twitter"></i></a>
                                    <a href="https://www.linkedin.com/company/haut-lac-international-bilingual-school" target="_blank"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container_full_size">
            <nav class="navbar navbar-default" role="navigation" id="navbar-sticky-wrapper">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo get_option('home'); ?>"><img src="<?= $base;?>/images/logo_ideogram.png"></a>
                    </div>

                    <?php wp_nav_menu( array(
                        'theme_location'	=> 'main_menu',
                        'sort_column'		=> 'menu_order',
                      //  'menu_class'		=> 'sf-menu',
                        'fallback_cb'		=> false,
                         //'menu' => 'primary',
                        //'theme_location' => 'primary',
                        'depth' => 2,
                        'container' => 'div',
                        /*   ' '=> 'test',*/
                        'container_class' => 'collapse navbar-collapse in',
                        'container_id' => 'bs-example-navbar-collapse-1',
                        'menu_class' => 'nav navbar-nav',
                    )); ?>

                </div>
            </nav>

        </div>

    </header>
    <div class="under_header_color">
        <div class="box_1"></div>
        <div class="box_2"></div>
        <div class="box_3"></div>
        <div class="box_4"></div>
    </div>
	<div id="main-content" class="clearfix">
        <div id="wrap" class="clearfix">